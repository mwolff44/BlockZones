########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
# Date: 2017/07/22
#
##
###
########################################################################
###
##
#   Texts
##
###
########################################################################

txt_answer_sure="¿Seguro que quieres salir ?"
txt_answer_without_signify="¿Quieres continuar sin firma (signify) de las listas ?"

txt_badip_file=" a los archivos badips."

txt_builded=" Parece ser construido!"

txt_check_ipv4="=> Verifica IPv4!"
txt_check_ipv6="=> Verifica IPv6!"

txt_choice="###	Su elección : "
txt_choice_bl="###	Su elección 'blacklist' : "

txt_created=" creado correctamente !"

txt_date="fecha : "

txt_deleted=" se elimina adecuadamente !"

txt_dialog_menu="Puede presionar el ALTO / ABAJO \n, o la primera opción que quieres. \n Elije una lista : "

txt_dlg_goodbye="\Zb Adios el extranjero ! \Zn \n\n El guión está terminado. \n\n Espero verte más tarde ! :D"
txt_dlg_ko="[ \Z1\Zb KO \Zn ] "
txt_dlg_ok="[ \Z2\Zb OK \Zn ] "
txt_dlg_welcome="\Zb Benvenudo : El guión de BlockZones! \Zn \n\n Estas ejecutando $0 por obtener la lista : ${list}. ${ARG}"

txt_download="=> Descargamiento : "
txt_download_file="=> Intento de descargar el archivo : "
txt_downloaded=" se ha cargado correctamente !"

txt_ended="- El guión est terminado !"

txt_error="Error : "
txt_error_create_file="Il semble y avoir un problème pour créer le fichier: "
txt_error_create_checksum_file="Il semble y avoir un problème pour créer le fichier de sommes de contrôle : "
txt_error_create_sign_file=" Il semble y avoir un problème pour créer le fichier de signature (signify) : "
txt_error_del_file="Il semble y avoir un problème pour supprimer le fichier : "
txt_error_download_file="Il semble y avoir un problème avec le fichier téléchargé : "
txt_error_move_checksum_file="Erreur pour déplacer le fichier : "
txt_error_no_data="Il semble ne pas y avoir de données !"
txt_error_no_file="Il semble ne pas y avoir de fichier : "
txt_error_not_build=" n'a pas été construit !"
txt_error_signify=" /!\ Il semble que l'outil signify ne soit pas disponible /!\ "
txt_error_unzip_tool=" /!\ Il semble que l'outil unzip ne soit pas disponible /!\ "

txt_execute="Vous exécutez "

txt_file="Le fichier "
txt_file_checksum="Le fichier de sommes de contrôle "
txt_file_sign=" Le fichier de signature "
txt_filename="Nom de fichier : "

txt_format="Format : "

txt_goodbye="Àdieu l'étranger!"

txt_hope="J'espère vous revoir plus tard... :p"

txt_into=" dans "
txt_in_list=" dans la liste "

txt_make_single_file="====> Essai de créer un fichier unique ayant pour nom de fichier : "

txt_menu_ps3="Veuillez choisir une action pour créer une liste : "
txt_menu_ps3_bl="Veuillez choisir une action pour créer une liste 'blacklist' : "

txt_menus_badips="Générer une liste 'badips'"
txt_menus_blacklist="Générer une liste 'blacklist'"
txt_menus_bogons="Générer une liste 'bogons'"
txt_menus_quit="Sortie"

txt_menus_bl_bind8="Générer une liste 'blacklist' pour le service bind v8.x"
txt_menus_bl_bind9="Générer une liste 'blacklist' pour le service bind v9.x"
txt_menus_bl_hosts="Générer une liste 'blacklist' pour /etc/hosts"
txt_menus_bl_pf="Générer une liste 'blacklist' pour Packet Filter"
txt_menus_bl_unbound="Générer une liste 'blacklist' pour le service 'unbound'"

txt_mng_list="Gérer la liste : "

txt_moved=" a correctement été déplacé vers "

txt_obtain=" pour obtenir "

txt_need_curl="Si vous installez l'outil 'curl', le script essaiera de l'utiliser la prochaine fois !"
txt_need_wget="Si vous installez l'outil 'wget', le script essaiera de l'utiliser la prochaine fois... à moins que 'curl' ne soit installé !"

txt_not_readable=" semble ne pas être lisible !"

txt_plz_install_tool="Veuillez installer l'outil : "
txt_purge_files="===> Essai de transformer le fichier téléchargé : "

txt_read_array_bl="### Essai de lire les données du tableau 'blocklist' !"

txt_search_reason="Veuillez chercher à en comprendre les raisons. "
txt_stop=" /!\ Le script s'arrête là /!\ "

txt_transform="=> Transformation : "
txt_transform_file="===> Essai de transformation en liste unique "

txt_wait="Veuillez attendre !"
txt_wait_download_file="Veuillez attendre quelques minutes durant la transformation du fichier : "
txt_wait_minutes="Veuillez attendre quelques minutes !"
txt_welcome="Bienvenue - BlockZones script!"
txt_without_signify="Le script continue sans générer de fichier de signature !"

txt_xtract_gz="==> Essai d'extraire l'archive .gz: "
txt_xtract_zip="==> Essai d'extraire l'archive .zip: "

txt_yn=" [o|n] "
