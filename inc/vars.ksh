########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
# Date: 2017/07/22
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

### DO NOT TOUCH!
OLD_TERM="$(echo $TERM)"
export TERM=xterm

### Detect Ksh Version; DO NOT TOUCH!
VKSH="$(echo $KSH_VERSION)"

### Declare directories; DO NOT TOUCH!
DIR_DL="${ROOT}/downloads"
DIR_INC="${ROOT}/inc"
DIR_LANG="${ROOT}/lang"
DIR_LISTS="${ROOT}/lists"
DIR_SRC="${ROOT}/src"

dir_pub_key_signify="${DIR_INC}/BlockZones_GenLists.pub"
dir_sec_key_signify="${DIR_INC}/BlockZones_GenLists.sec"

### to get checksums files into on file. 
# If '0', one checksum file by list
# If '1', one chechsum file for all lists
typeset -i one_checksum_file=1

### to detect tools; DO NOT TOUCH!
typeset -i use_curl=0
typeset -i use_sign=1
typeset -i use_wget=0

### declare OS variables; DO NOT TOUCH!
OSN="$(uname -s)"	# Get Operating System Name
OSR="$(uname -r)"	# Get Operating System Release

### declare color variables; DO NOT TOUCH!
bold="$(tput bold)"
dim="$(tput dim)"
green="$(tput setaf 2)"
neutral="$(tput sgr0)"
red="$(tput setaf 1)"

### others variables; DO NOT TOUCH!
choice=""	# variable choice to value menu
list=""		# list name

### modes; modify with precautions!
#typeset -i cpgb=0		# set counter for progress bar into dialog; DO NOT TOUCH!
typeset -i debug=0		# set debug mode
typeset -i dialog=0		# set dialog mode
typeset -i verbose=0	# set verbose mode

### dates; DO NOT TOUCH
now="$(date +"%x %X")"
today="$(date +%s)"

typeset -i seconds=86400   #  default delay, in seconds, before downloads list, again. Modify only with precautions!

### lang; DO NOT TOUCH!
lang="$(echo ${LANG})"
[ -z "${lang}" ] && lang="$(echo ${LC_MESSAGES})"
[ -z "${lang}" ] && lang="en"
lang="$(echo "${lang}" | awk '{ print substr($0,0,2) }')"
#[[ "$VKSH" == *"PD KSH"* ]] && lang="$(printf '%.2s\n' "${lang}")" || lang="${lang:0:2}"	# substring two first caracters

# few arrays; DO NOT TOUCH!
set -A blocklists
set -A FILES
set -A menus 'badips' 'blacklist' 'bogons' 'quit'
set -A menus_blacklists 'unbound' 'bind8' 'bind9' 'hosts' 'pf'
