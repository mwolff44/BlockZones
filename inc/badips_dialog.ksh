########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
# Date: 2017/07/16
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

list="badips"

FILES[0]="${DIR_SRC}/${list}_ipv4"
FILES[1]="${DIR_SRC}/${list}_ipv6"

########################################################################
###
##
#    Functions
##
###
########################################################################

. "${DIR_INC}/commons_functions.ksh"

# Create uniq list file by datas into array blocklist
mng_blocklists() {
	
    dialog --backtitle "${txt_project_name}" --title "${ttl_manager}" --infobox "${txt_read_array_bl}" 10 100

    count="${#blocklists[@]}" 
	modulo=$(echo "100/$count"|bc)

    if [ "${count}" -gt 0 ]; then
    
    	(
				
		#let counter+=$modulo
		#[ $counter -ge 100 ] && break

		for url in "${blocklists[@]}"; do

			ndd="$(echo "${url}" | awk -F'/' '{ print $3 }')"
			file="$(echo "${url##*/}" | sed -e 's#\?#_#g;s#=#_#g;s#php#txt#g;s#\&#_#g')";
			name="${ndd}_${file}"
			filename="${DIR_DL}/${name}"
				
			let x+=$modulo
				
			cat <<EOF
XXX
$x
$txt_mng_list $list ($x%)
$txt_download $url
$txt_transform $name
XXX
EOF
				
			[ "${debug}" -eq 1 ] && printf "file: %s \n" "${file}"

			# define seconds before new dl
			case "${ndd}" in
				"feeds.dshield.org") seconds=259200 ;;	# 3 days
				"lists.blocklist.de") seconds=172800 ;;	# 2 days
				"myip.ms") seconds=864000 ;;	# 10 days
				"ransomwaretracker.abuse.ch") seconds=2592000 ;;	# 30 days
				#"sslbl.abuse.ch") seconds=900 ;; # 15 min.
				#"www.openbl.org") seconds=172800 ;;	# 2 days
				"www.spamhaus.org") seconds=3600;; # 1 hours
				#*) seconds=86400;;
			esac

			if [ -f "${filename}" ]; then

				# get file seconds stat
				file_seconds=$(stat -f "%m" -t "%s" "${filename}")

				# calcul diff time in seconds
				diff_sec=$(echo "${today} - ${file_seconds}" | bc)

				#unset file_seconds

				if [ ${diff_sec} -gt ${seconds} ]; then download; fi

			else

				download

			fi

			case "${ndd}" in
				"www.openbl.org")
					uncompress
				
					filename="${filename%.gz}"
				;;
			esac
			
			purge_files

			make_uniq_list

			unset filename name ndd file
            
		done
			
		) | dialog --backtitle "${txt_project_name}" --title "${ttl_manager}" --gauge "${txt_wait}" 10 100 

    else

        dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_manager}" --infobox "${txt_dlg_ko}${txt_error_no_data}" 7 100
        sleep 1
        
        byebye

    fi
    
    sleep 1

    unset count

}

purge_files() {

    #[ "${verbose}" -eq 1 ] && display_mssg "hb" "===> Attempt to transform downloaded file: ${filename}"

    # /^$/d     <= empty line
    # /^#/d     <= line starting with #
    # /^\s*$/d  <= remove spaces'n tabs; /^[[:space:]]*$/d
    # s/ \+//g  <= remove all spaces
    # s/ \+/ /g  <= replace more spaces by only one

    case "${ndd}" in
		"danger.rulez.sk")  # delete line ^#, and keep only adr ip
            #sed -i -e "/^#/d;s/^\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\t.*/\1/g;s/ \+//g" "${filename}"
            sed -i -e "/^#/d;s/\(.*\)#\(.*\)/\1/g" "${filename}"
        ;;
        "feeds.dshield.org")    # delete empty lines, and ^#
            #sed -i -e "/^$/d;s#^Start\(.*\)#\#Start\1#g;/^#/d;s#^\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\t.*#\1#g;s/ \+//g" "${filename}"
            sed -i -e "/^$/d;s#^Start\(.*\)#\#Start\1#g;/^#/d;s/ \+//g" "${filename}"
            
            rm -f "${filename}_awk"
            
            while read -r line; do
				echo "${line}" | awk '{print $1"/"$3}' >> "${filename}_awk"
            done < "${filename}"
            
            filename="${filename}_awk"
            #sed -i -e "s#\(.*\)\/$#\1#g" "${filename}"
            
        ;;
		"malc0de.com")  # delete empty lines, and ^//
            sed -i -e "/^$/d;/^\/\//d;s/ \+//g" "${filename}"
        ;;
        "myip.ms")  # delete empty lines, spaces, and keep only adr ip
            sed -i -e "/^$/d;/^#/d;s/\(.*\)#\(.*\)/\1/g;s/ \+//g" "${filename}"
        ;;
        "sslbl.abuse.ch")   # delete empty lines, ^#, ...
            #sed -i -e "/^$/d;/^#/d;s#^\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\,.*#\1#g;s/ \+//g" "${filename}"
            sed -i -e "/^$/d;/^#/d;s#\(.*\),\(.*\),\(.*\)#\1#g;s/ \+//g" "${filename}"
        ;;
		"www.spamhaus.org") # delete empty lines, ...
            sed -i -e "/^\;/d;s/ \+//g;s#\(.*\) \; SBL\(.*\)#\1#g" "${filename}"
        ;;
        *)  # delete empty lines, ^#, keep all others :p
            sed -i -e "/^$/d;/^#/d;s/ \+//g" "${filename}"
        ;;
    esac

}

transformer() {
	
	count="${#FILES[@]}"
	modulo=$(echo "100/$count"|bc)
	
	(
	
	for f in "${FILES[@]}"; do
		
		if [ -f "${f}" ]; then
		
			let x+=$modulo
				
			cat <<EOF
XXX
$x
$txt_transform $list ($x%)
$txt_file $f
XXX
EOF
			
			output="$(basename "${f}")"
			
			mv "${f}" "${DIR_LISTS}/${output}"; 
			
			build_sums
			
			unset output
			
		else
		
			dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_transformer}" --infobox "${txt_dlg_ko}${txt_error_no_file}${f}" 10 100
		
		fi
		
		
    done
    ) | dialog --backtitle "${txt_project_name}" --title "${ttl_transformer}" --gauge "${txt_wait}" 10 100 

	sleep 1
	
}
