########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
# Date: 2017/07/22
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################
### Ty to @kuniyoshi for FreeBSD Codes - 2017/07/10
########################################################################

# Get languages texts
[ -f "${DIR_LANG}/texts.${lang}" ] && . "${DIR_LANG}/texts.${lang}" || . "${DIR_LANG}/texts.en" 
[ -f "${DIR_LANG}/titles.${lang}" ] && . "${DIR_LANG}/titles.${lang}" || . "${DIR_LANG}/titles.en" 

### Get data into file in array...
build_blocklists() {

    [ "${verbose}" -eq 1 ] && display_mssg "#" "*** ${txt_mng_list} ${DIR_SRC}/${list} ***"

    if [ -f "${DIR_SRC}/${list}" ]; then

        i=0
        while read -r line; do
        
            if [ "${verbose}" -eq 1 ]; then
				if echo "${line}" | grep -v "^#"; then blocklists[$i]="${line}"; fi
			
			else
				if echo "${line}" | grep -qv "^#"; then blocklists[$i]="${line}"; fi
				
			fi
            let i++
        
        done < "${DIR_SRC}/${list}"
        
        unset i

    else

        display_mssg "KO" "${txt_file}${DIR_SRC}/${list}${txt_not_readable}"
        
        byebye

    fi

}

### Create sha512 checksums files!
build_sums() {
	
	if [ "${one_checksum_file}" = 0 ]; then 
	
		typeset bool=1
	
		case "${list}" in
			"bogons") output="${file}" ;;
		esac

		if [ -f "${DIR_LISTS}/${output}" ];  then

			cd "${DIR_LISTS}" || exit 1
        
			case "${OSN}" in 
        
				"FreeBSD")
					if sha512 "${output}" > "${output}.sha512"; then bool=0; fi
				;;
        
				"OpenBSD")
					if sha512 -h "${output}.sha512" "${output}"; then bool=0; fi
				;;
			
			esac
        
			if [ ${bool} -eq 0 ]; then
        
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${title_checksums}" --infobox "${txt_dlg_ok}${txt_file_checksum}'${DIR_LISTS}/${output}'${txt_created}" 7 100
					sleep 1
				else
					display_mssg "OK" "${txt_file_checksum}'${DIR_LISTS}/${output}'${txt_created}"
				fi
			
				create_sign
        
			else
		
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${title_checksums}" --infobox "${txt_dlg_ko}${txt_error_create_checksum_file}${DIR_LISTS}/${output}" 7 100
					sleep 1
				else
					display_mssg "KO" "${txt_error_create_checksum_file}${DIR_LISTS}/${output}"
				fi
        
			fi
        
			cd "${ROOT}" || exit 1

		fi
    
		unset bool
		
	fi

}

byebye() {

	if [ ${dialog} -eq 1 ]; then
		
		dialog --colors --backtitle "${ttl_project_name}" --title "${title_byebye}" --msgbox "${txt_dlg_ko}${txt_stop} \n\n ${txt_search_reason}" 10 100
		
	else
	
		display_mssg "KO" "${txt_stop}"
		display_mssg "KO" "${txt_search_reason}"
		
	fi
    
    exit 1;

}

check_needed_softs() {
	
	# curl 
	if [ -f /usr/local/bin/curl ]; then 
		use_curl=1
	
	else
		display_mssg "hg" "${txt_need_curl}"
		sleep 1
	
	fi
	
	# wget
	if [ -f /usr/local/bin/wget ]; then 
		use_wget=1
	
	else
		display_mssg "hg" "${txt_need_wget}"
		sleep 1
	
	fi
	
	# unzip
	if [ ! -f /usr/local/bin/unzip ]; then 
		display_mssg "KO" "${txt_error_unzip_tool}"
		display_mssg "#" "${txt_plz_install_tool}unzip"
		byebye
	fi
	
	# signify
	if [ "${OSN}" = "FreeBSD" ]; then
		if [ ! -f /usr/local/bin/signify ]; then 
			use_sign=0
			
			display_mssg "KO" "${txt_error_signify}"
			
			if confirm "${txt_answer_without_signify}"; then
				display_mssg "#" "${txt_without_signify}"
				
				sleep 1
			else
				display_mssg "#" "${txt_plz_install_tool}signify"
				byebye
			fi
		fi
	fi
	
}


confirm () {

    read -r response?"${1}${txt_yn}"
    case "$response" in
        y|Y|o|O|1)	# O is not zero: O(ui) ;)
            true
            ;;
        *)
            false
            ;;
    esac

}

create_one_sums() {
	
	if [ "${one_checksum_file}" = 1 ]; then
	
		cd "${DIR_LISTS}" || exit 1
	
		find ./ -exec sha512 {} + > "${DIR_SRC}/BlockZones.sha512"; 
	
		if [ -f "${DIR_SRC}/BlockZones.sha512" ]; then
	
			if [ ${dialog} -eq 1 ]; then
				dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_create_checksum}" --infobox "${ttx_dlg_ok}${txt_file_checksum}'${DIR_SRC}/BlockZones.sha512'${txt_created}" 7 100
				sleep 1
			else
				display_mssg "OK" "${txt_file_checksum}'${DIR_SRC}/BlockZones.sha512'${txt_created}"
			fi
		
			if mv "${DIR_SRC}/BlockZones.sha512" "${DIR_LISTS}"; then
			
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_create_checksum}s" --infobox "${ttx_dlg_ok}${txt_file_checksum}'${DIR_SRC}/BlockZones.sha512'${txt_moved}${DIR_LISTS}" 7 100
					sleep 1
				else
					display_mssg "OK" "${txt_file_checksum}'${DIR_SRC}/BlockZones.sha512'${txt_moved}${DIR_LISTS}"
				fi
			
				create_sign
			
			else
			
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_create_checksum}" --infobox "${txt_dlg_ko}${txt_error_move_checksum_file}'${DIR_SRC}/BlockZones.sha512'${txt_into}${DIR_LISTS}" 7 100
					sleep 1
				else
					display_mssg "KO" "${txt_error_move_checksum_file}'${DIR_SRC}/BlockZones.sha512'${txt_into}${DIR_LISTS}"
				fi
			
			fi
	
		else
		
			if [ ${dialog} -eq 1 ]; then
				dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_create_checksum}" --infobox "${txt_dlg_ko}${txt_error_create_checksum_file}'${DIR_SRC}/BlockZones.sha512'" 7 100
				sleep 1
			else
				display_mssg "KO" "${txt_error_create_checksum_file}'${DIR_SRC}/BlockZones.sha512'"
			fi
			
		fi
	
		cd "${ROOT}" || exit 1

	fi
	
}

create_sign() {
	
	if [ "${use_sign}" = 1 ]; then
	
		if [ "${one_checksum_file}" = 1 ]; then 
			if signify -S -s "${dir_sec_key_signify}" -m "${DIR_LISTS}/BlockZones.sha512" -e -x "${DIR_LISTS}/BlockZones.sha512.sig"; then
			
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_create_sign}" --infobox "${txt_dlg_ok}${txt_file_sign}'${DIR_LISTS}/BlockZones.sha512.sig'${txt_created}" 7 100
					sleep 1
				else
					display_mssg "OK" "${txt_file_sign}'${DIR_LISTS}/BlockZones.sha512.sig'${txt_created}"
				fi
	
			else
				
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_create_sign}" --infobox "${txt_dlg_ko}${txt_error_create_sign_file}'${DIR_LISTS}/BlockZones.sha512.sign'" 7 100
					sleep 1
				else
					display_mssg "KO" "${txt_error_create_sign_file}'${DIR_LISTS}/BlockZones.sha512.sign'"
				fi
		
			fi
		
		else
			if signify -S -s "${dir_sec_key_signify}" -m "${DIR_LISTS}/${output}.sha512" -e -x "${DIR_LISTS}/${output}.sha512.sig"; then
				
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_create_sign}" --infobox "${txt_dlg_ok}${txt_file_sign}'${DIR_LISTS}/${output}.sha512.sig'${txt_created}" 7 100
					sleep 1
				else
					display_mssg "OK" "${txt_file_sign}'${DIR_LISTS}/${output}.sha512.sig'${txt_created}"
				fi
	
			else
				
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_create_sign}" --infobox "${txt_dlg_ko}${txt_error_create_sign_file}'${DIR_LISTS}/${output}.sha512.sign'" 7 100
					sleep 1
				else
					display_mssg "KO" "${txt_error_create_sign_file}'${DIR_LISTS}/${output}.sha512.sign'"
				fi
		
			fi
		
		fi
	
	fi
	
}

### To delete files
del_files() {
	
	[ "${verbose}" -eq 1 ] && display_mssg "#" "${ttl_del_files}"
	
	for f in "${FILES[@]}"; do
	
		[ "${verbose}" -eq 1 ] && echo "${txt_file}${f}"
		
		if [ -f "${f}" ]; then
		
			if rm "${f}"; then
				[ "${verbose}" -eq 1 ] && display_mssg "OK" "${txt_file}${f}${txt_deleted}"
				
			else
				display_mssg "KO" "${txt_error_del_file}${f}"
				
			fi
		
		fi
		
	done
	
	del_sums
	
}

### To delete spaces, lines into files
del_spaces() {
	
	[ "${verbose}" -eq 1 ] && display_mssg "#" "${ttl_del_spaces}"

	# delete empties spaces and lines...
    for f in "${FILES[@]}"; do
    
		{ rm "${f}" && sed -e "/^$/d;/^[[:space:]]*$/d" | sort -du -o "${f}"; } < "${f}"
    
    done
    
}

del_sums() {
	
	if [ "${one_checksum_file}" = 1 ]; then
	
		find "${DIR_LISTS}/" -type f -name "*\.sha512*" -exec rm -f {} +
	
	else
		rm -f "${DIR_LISTS}/BlockZones.sha512*"
	
	fi
	
}

display_mssg() {

    typeset statut info 
    statut="$1" info="$2" 
    
    case "${statut}" in
        "KO"|1)	color="${red}" 		;;
        "OK"|0)	color="${green}" 	;;
    esac

	if [ "${statut}" == "#" ]; then
		if [ "${OSN}" = "FreeBSD" ]; then
			printf "%s" "[ "; tput bold; printf "%s" "${info}"; tput sgr0; printf "%s" "] \n";
			
		else
			printf "[ ${bold}%s${neutral} ] \n" "${info}"
			
		fi
	
	elif [ "${statut}" == "hb" ]; then
		if [ "${OSN}" = "FreeBSD" ]; then
			tput dim; printf "%s \n" "${info}"; tput sgr0; printf "\n"
			
		else
			printf "${dim}%s${neutral} \n" "${info}"
			
		fi
		
	else
		if [ "${OSN}" = "FreeBSD" ]; then
			case "${statut}" in
				"KO")
					printf "%s" "[ "; tput setaf 1; printf "%s" "KO"; tput sgr0; printf "%s \n" " ] ${info}"
				;;
				"OK")
					printf "%s" "[ "; tput setaf 2; printf "%s" "OK"; tput sgr0; printf "%s \n" " ] ${info}"
				;;
			esac
			
		else
			printf "[ ${color}%s${neutral} ] %s \n" "${statut}" "${info}"
			
		fi
	
	fi

    unset info statut text

}

display_mssg_end() {
	
	if [ ${dialog} -eq 1 ]; then 
	
		dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_project_name}" --msgbox "${txt_dlg_goodbye}" 10 100
		
		dialog --clear
		clear
	
	else
	
		echo "
########################################################################
####
##
#   ${txt_goodbye}
#	${txt_ended}
#   ${txt_hope}
##
###
########################################################################
"
	
	fi
	
	exit 0
	
}

display_welcome() {
	
	if [ ${dialog} -eq 1 ]; then 

		if dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_project_name}" --yesno "${txt_dlg_welcome}" 10 100; then
			sleep 1
		
		else
			display_mssg_end
			
		fi

	else
		
		echo "
########################################################################
####
##
#   ${txt_welcome}
##
###
##
#	${txt_execute}$0${txt_obtain}${list}
##
###
########################################################################
"
	fi

}

### Downloader needed files
download() {

    [ "${verbose}" -eq 1 ] && display_mssg "#" "${ttl_download}"
    [ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_download_file}${filename}"

    typeset bool=0 options=""

    if [ "${use_curl}" = 1 ]; then
    
		if [ ${dialog} -eq 1 ]; then options="-s "; fi
		
		if [ ${verbose} -eq 1 ] || [ ${debug} -eq 1 ]; then options="-v "; fi
		
		if ! /usr/local/bin/curl -A "Mozilla/5.0 (X11; Debian; Linux; rv:0.0) Gecko/20100101 Firefox/0.0" --compressed ${options} -o "${filename}" "${url}"; then
			bool=1
		fi

    elif [ "$(use wget)" = 1 ]; then

		if [ ${dialog} -eq 1 ]; then options+="-q "; fi
		
		if [ ${debug} -eq 1 ]; then options+="-d "; fi
					
		if ! /usr/local/bin/wget --user-agent="Mozilla/5.0 (X11; Debian; Linux; rv:0.0) Gecko/20100101 Firefox/0.0" -c "${options}" -O "${filename}" "${url}"; then
			bool=1
		fi

    else
    
		options="-C -n -m"
    
		if [ ${dialog} -eq 1 ]; then options="-C -n -V"; fi

		if [ ${debug} -eq 1 ]; then options="-C -d -n -m "; fi
		
		if ! /usr/bin/ftp "${options}" -o "${filename}" "${url}"; then bool=1; fi

    fi

    if [ ${bool} -eq 0 ]; then

        [ "${verbose}" -eq 1 ] && display_mssg "OK" "${txt_file}${filename}${txt_downloaded}"

    else

        display_mssg "KO" "${txt_error_download_file}${filename}!"
        byebye

    fi
    
    unset bool

}

get_array_menu() {
	
	case "${choice}" in 
	
		"blacklist")
			set -A NAMES -- "${menus_blacklists[@]}"
		;;
	
		*)
			set -A NAMES -- "${menus[@]}"
		;;
		
	esac
	
	typeset -i nb="${#NAMES[@]}" i=0 j=0
	
	while (( i < $nb )); do
	
		name="${NAMES[$i]}"
		
		if [ "${choice}" == "blacklist" ]; then 
			text="txt_menus_bl_$name"
		else
			text="txt_menus_$name"
		fi
		
		#[[ "$VKSH" == *"PD KSH"* ]] && eval "ref=\${$text}" || typeset -n ref="${text}"
		eval "ref=\${$text}"
		
		dialog_menu[$j]="${NAMES[$i]}"
		dialog_menu[$j+1]="${ref}"
		
		let j+=2
		let i+=1
		
		unset ref text name
		
	done
	
}

make_uniq_list() {
    
    touch_files

    [ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_make_single_file}${filename}"
    
    case "${list}" in
		"badips")
		
			[ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_check_ipv4}"
			valid_ipv4 "${filename}" >> "${DIR_SRC}/${list}_ipv4"
        
			[ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_check_ipv6}"
			valid_ipv6 "${filename}" >> "${DIR_SRC}/${list}_ipv6"
		
		;;
		"bogons")
		
			case "${file}" in
				"fullbogons-ipv4.txt")
		
					[ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_check_ipv4}"
					valid_ipv4 "${filename}" >> "${DIR_LISTS}/${file}"
		
				;;
				"fullbogons-ipv6.txt")
    
					[ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_check_ipv6}"
					valid_ipv6 "${filename}" >> "${DIR_LISTS}/${file}"
	
				;;
		
			esac
			
		;;
		"domains")
			
			awk '{ print tolower($0) }' "${filename}" >> "${DIR_SRC}/uniq_${list}"
			
		;;
	esac

	del_spaces

}

menu() {
	
	display_welcome
	
	PS3="${txt_menu_ps3}"
	select option in "${menus[@]}"; do
		choice="$option"; break
	done
	
	echo "
${txt_choice}${choice}
	"
	
	case "${choice}" in
		
		"blacklist") 
		choice="blacklists"
	
		PS3="${txt_menu_ps3_bl}"
		select option in "${menus_blacklists[@]}"; do
			choice_bl="${option}"; break
		done
		
		echo "
${txt_choice_bl}${choice_bl}	
		"
		;;
		
		"quit") display_mssg_end ;;
	
	esac
	
	sleep 1
	
}

menu_dialog() {
	
	if [ -z "$1" ]; then
		set -A dialog_menu
		INPUT=/tmp/BZ_menu.sh.$$
	
		get_array_menu
	
		dialog --clear --backtitle "${ttl_project_name}" --title "${ttl_menu}" \
		--menu "${txt_dialog_menu}" 0 0 10 "${dialog_menu[@]}" 2> "${INPUT}"

		return=$?
		choice="$(cat "${INPUT}" | tr [:upper:] [:lower:])"
	
		unset dialog_menu INPUT
	
		case "${return}" in
			1) display_mssg_end ;;
		esac
	
		if [ -z "${choice}" ]; then menu_dialog; fi
	else 
		choice="$1"
	fi
	
	case "${choice}" in
		
		"blacklist")
			set -A dialog_menu
			INPUT=/tmp/BZ_BL_menu.sh.$$
			
			get_array_menu
			
			dialog --clear --backtitle "${ttl_project_name}" --title "${ttl_menu_blacklist}" \
			--menu "${txt_dialog_menu}" 0 0 10 "${dialog_menu[@]}" 2> "${INPUT}"
			
			return=$?
			choice_bl="$(cat "${INPUT}" | tr [:upper:] [:lower:])"
			
			case "${return}" in
				1) 
					if dialog --backtitle "${ttl_project_name}" --clear --title "${ttl_menu_blacklist}" --yesno "${txt_answer_sure}" 10 100; then
						display_mssg_end
					else
						menu_dialog "${choice}"
					fi
				;;
			esac
			
			choice="blacklists"
			
			unset dialog_menu INPUT
		;;
		
		"quit") display_mssg_end ;;
		
	esac
	
}

### To created files
touch_files() {
	
	[ "${verbose}" -eq 1 ] && display_mssg "#" "${ttl_touch_files}"
	
	for f in "${FILES[@]}"; do
		
		if [ ! -f "${f}" ]; then
		
			if touch "${f}"; then
				display_mssg "OK" "${txt_file}${f}${txt_created}"
				
			else
				display_mssg "KO" "${txt_error_create_file}${f}"
				
			fi
		
		fi
		
	done
	
}

### extract archive
uncompress() {

    if [ "$(file -b -i "${filename}")" = "application/gzip" ]; then
        [ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_xtract_gz}${filename}"
        /usr/bin/gunzip -d -f -q "${filename}";
    fi
    
    if [ "$(file -b -i "${filename}")" = "application/x-gzip" ]; then
        [ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_xtract_gz}${filename}"
        /usr/bin/gunzip -d -f -q "${filename}";
    fi

    if [ "$(file -b -i "${filename}")" = "application/zip" ]; then
        [ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_xtract_zip}${filename}"
        /usr/local/bin/unzip -oqu "${filename}" -d "${filename%.zip}"
    fi

}

# functions valid_ip**() inspired 
# by: https://helloacm.com/how-to-valid-ipv6-addresses-using-bash-and-regex/
valid_ipv4() {
	
	egrep '^([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\/?([0-9]{1,2})?)$' "${1}"
	
}
	
valid_ipv6() {

	egrep '^([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])$' "${1}"
	
}
