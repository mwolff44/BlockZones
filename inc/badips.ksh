########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
# Date: 2017/07/16
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

list="badips"

FILES[0]="${DIR_SRC}/${list}_ipv4"
FILES[1]="${DIR_SRC}/${list}_ipv6"

########################################################################
###
##
#    Functions
##
###
########################################################################

. "${DIR_INC}/commons_functions.ksh"

# Create uniq list file by datas into array blocklist
mng_blocklists() {

    [ "${verbose}" -eq 1 ] && display_mssg "#" "${txt_read_array_bl}"

    count="${#blocklists[@]}"

    if [ "${count}" -gt 0 ]; then

        for url in "${blocklists[@]}"; do

			ndd="$(echo "${url}" | awk -F'/' '{ print $3 }')"
			file="$(echo "${url##*/}" | sed -e 's#\?#_#g;s#=#_#g;s#php#txt#g;s#\&#_#g')";
			name="${ndd}_${file}"
			filename="${DIR_DL}/${name}"

            [ "${debug}" -eq 1 ] && printf "file: %s \n" "${file}"

            # define seconds before new dl
            case "${ndd}" in
				"feeds.dshield.org") seconds=259200 ;;	# 3 days
				"lists.blocklist.de") seconds=172800 ;;	# 2 days
				"myip.ms") seconds=864000 ;;	# 10 days
				"ransomwaretracker.abuse.ch") seconds=2592000 ;;	# 30 days
				#"sslbl.abuse.ch") seconds=900 ;; # 15 min.
				#"www.openbl.org") seconds=172800 ;;	# 2 days
                "www.spamhaus.org") seconds=3600;; # 1 hours
                #*) seconds=86400;;
            esac

            if [ -f "${filename}" ]; then

                # get file seconds stat
                file_seconds=$(stat -f "%m" -t "%s" "${filename}")

                # calcul diff time in seconds
                diff_sec=$(echo "${today} - ${file_seconds}" | bc)

                #unset file_seconds

                if [ ${diff_sec} -gt ${seconds} ]; then download; fi

            else

                download

            fi

            case "${ndd}" in
				"www.openbl.org")
					uncompress
					
					filename="${filename%.gz}"
				;;
            esac
			
			purge_files

            make_uniq_list

            unset filename

        done

    else

        display_mssg "KO" "${txt_error_no_data}"
        
        byebye

    fi

    unset count

}

purge_files() {

    [ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_purge_files}${filename}"

    # /^$/d     <= empty line
    # /^#/d     <= line starting with #
    # /^\s*$/d  <= remove spaces'n tabs; /^[[:space:]]*$/d
    # s/ \+//g  <= remove all spaces
    # s/ \+/ /g  <= replace more spaces by only one

    case "${ndd}" in
		"danger.rulez.sk")  # delete line ^#, and keep only adr ip
            #sed -i -e "/^#/d;s/^\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\t.*/\1/g;s/ \+//g" "${filename}"
            sed -i -e "/^#/d;s/\(.*\)#\(.*\)/\1/g" "${filename}"
        ;;
        "feeds.dshield.org")    # delete empty lines, and ^#
            #sed -i -e "/^$/d;s#^Start\(.*\)#\#Start\1#g;/^#/d;s#^\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\t.*#\1#g;s/ \+//g" "${filename}"
            sed -i -e "/^$/d;s#^Start\(.*\)#\#Start\1#g;/^#/d;s/ \+//g" "${filename}"
            
            rm -f "${filename}_awk"
            
            while read -r line; do
				echo "${line}" | awk '{print $1"/"$3}' >> "${filename}_awk"
            done < "${filename}"
            
            filename="${filename}_awk"
            #sed -i -e "s#\(.*\)\/$#\1#g" "${filename}"
            
        ;;
		"malc0de.com")  # delete empty lines, and ^//
            sed -i -e "/^$/d;/^\/\//d;s/ \+//g" "${filename}"
        ;;
        "myip.ms")  # delete empty lines, spaces, and keep only adr ip
            sed -i -e "/^$/d;/^#/d;s/\(.*\)#\(.*\)/\1/g;s/ \+//g" "${filename}"
        ;;
        "sslbl.abuse.ch")   # delete empty lines, ^#, ...
            #sed -i -e "/^$/d;/^#/d;s#^\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\,.*#\1#g;s/ \+//g" "${filename}"
            sed -i -e "/^$/d;/^#/d;s#\(.*\),\(.*\),\(.*\)#\1#g;s/ \+//g" "${filename}"
        ;;
		"www.spamhaus.org") # delete empty lines, ...
            sed -i -e "/^\;/d;s/ \+//g;s#\(.*\) \; SBL\(.*\)#\1#g" "${filename}"
        ;;
        *)  # delete empty lines, ^#, keep all others :p
            sed -i -e "/^$/d;/^#/d;s/ \+//g" "${filename}"
        ;;
    esac

}

transformer() {
	
	#for f in "${DIR_SRC}/${list}_ipv4" "${DIR_SRC}/${list}_ipv6"; do
	for f in "${FILES[@]}"; do
	
		[ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_transform_file}'${f}'${txt_badip_file}"
		
		if [ -f "${f}" ]; then
			
			output="$(basename "${f}")"
			
			if mv "${f}" "${DIR_LISTS}/${output}"; then 
				display_mssg "OK" "${txt_file}'${DIR_LISTS}/${output}'${txt_builded}"
			fi
			
			build_sums
			
			unset output
			
		else
		
			display_mssg "KO" "${txt_error_no_file}'${f}'"
		
		fi
		
		
    done

}
