########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
# Date: 2017/07/16
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

list="domains"

FILES[0]="${DIR_SRC}/uniq_${list}"

IPv4=1	# enable or not IPV4 management - use by hosts, unbound
IPv6=1	# enable or not IPv6 management - use by hosts, unbound

USE_LZ_REDIRECT=0	# enable or not to use local-zone redirect for unbound

ARG="${1-ARG}"
if [[ "$ARG" = "ARG" ]]; then
    ARG="unbound"
fi
# if menu run.
if [ "${choice}" = "blacklists" ]; then
	if [[ ${menus_blacklists[@]} == *"${choice_bl}"* ]]; then ARG="${choice_bl}"; fi
	if [ -z "${ARG}" ]; then ARG="unbound"; fi
fi

########################################################################
###
##
#    Functions
##
###
########################################################################

. "${DIR_INC}/commons_functions.ksh"

build_uniq_list() {

    # on s'assure d'une liste de noms uniques
    ## http://promberger.info/linux/2009/01/14/removing-duplicate-lines-from-a-file/

    #{ rm "${DIR_SRC}/uniq_${list}" && awk '!x[tolower($1)]++' > "${DIR_SRC}/uniq_${list}"; } < "${DIR_SRC}/uniq_${list}"
    # shellcheck disable=SC2094
    { rm "${FILES[0]}" && awk '!x[tolower($1)]++' | sort -du -o "${FILES[0]}"; } < "${FILES[0]}"

}

# Create uniq list file by datas into array blocklist
mng_blocklists() {

    dialog --backtitle "${txt_project_name}" --title "${ttl_manager}" --infobox "${txt_read_array_bl}" 10 100

    count="${#blocklists[@]}"
    modulo=$(echo "100/$count"|bc)

    if [ "${count}" -gt 0 ]; then
    
		(

        for url in "${blocklists[@]}"; do

			if [ "${url}" = "personals" ]; then
				filename="${DIR_SRC}/${url}"
			else
				ndd="$(echo "${url}" | awk -F'/' '{ print $3 }')"
				file="$(echo "${url##*/}" | sed -e 's#\?#_#g;s#=#_#g;s#php#txt#g;s#\&#_#g')";
				name="${ndd}_${file}"
				filename="${DIR_DL}/${name}"
			fi
            [ "${debug}" -eq 1 ] && printf "file: %s \n" "${file}"
            
            let x+=$modulo
				
			cat <<EOF
XXX
$x
$txt_mng_list $list ($x%)
$txt_download $url
$txt_transform $name
XXX
EOF

            # define seconds before new dl
            case "${ndd}" in
                "mirror1.malwaredomains.com") seconds=2592000;;   # 1 month
                "winhelp2002.mvps.org") seconds=604800;; # 7 days
                "www.spamhaus.org") seconds=3600;; # 1 hours
                #*) seconds=86400;;
            esac

            if [ -f "${filename}" ]; then

                # get file seconds stat
                file_seconds=$(stat -f "%m" -t "%s" "${filename}")

                # calcul diff time in seconds
                diff_sec=$(echo "${today} - ${file_seconds}" | bc)

                unset file_seconds

                if [ ${diff_sec} -gt ${seconds} ]; then 
                
					if [ "${url}" != "personals" ]; then download; fi
				
				fi

            else

                if [ "${url}" != "personals" ]; then download; fi

            fi

            uncompress

            case "${ndd}" in
                "hosts-file.net")
                    if [ "$(file -b -i "${filename}")" = "application/zip" ]; then
                        filename="${filename%.zip}/hosts.txt"
                    fi
                ;;
                "mirror1.malwaredomains.com")
                    if [ "$(file -b -i "${filename}")" = "application/zip" ]; then
                        case "${file}" in
                            "immortal_domains.zip")
                                filename="${filename%.zip}/${file%.zip}.txt"
                            ;;
                            "justdomains.zip"|"malwaredomains.zones.zip")
                                filename="${filename%.zip}/${file%.zip}"
                            ;;
                        esac
                    fi
                ;;
                "winhelp2002.mvps.org")
                    if [ "$(file -b -i "${filename}")" = "application/zip" ]; then
                        filename="${filename%.zip}/HOSTS"
                    fi
                ;;
            esac

            purge_files

            make_uniq_list

            unset filename

        done
        
        ) | dialog --backtitle "${txt_project_name}" --title "${ttl_manager}" --gauge "${txt_wait}" 10 100 

    else

        dialog --backtitle "${txt_project_name}" --colors --title "${ttl_manager}" --infobox "${txt_dlg_ko}${txt_error_no_data}" 7 100
        sleep 1
        
        byebye

    fi

    unset count

}

purge_files() {

    #[ "${verbose}" -eq 1 ] && display_mssg "hb" "===> Attempt to transform downloaded file: ${filename}"

    # /^$/d     <= empty line
    # /^#/d     <= line starting with #
    # /^\s*$/d  <= remove spaces'n tabs; /^[[:space:]]*$/d
    # s/ \+//g  <= remove all spaces
    # s/ \+/ /g  <= replace more spaces by only one

    case "${ndd}" in
        "adaway.org")
            case "${ARG}" in
                "host"|"hosts"|"host0")
                    sed -i -e "/^$/d;/^#/d;s/\(.*\) ##\(.*\)#/\1/g;/\(.*\)localhost\(.*\)/d;" "${filename}"
                ;;
                *)
                    sed -i -e "/^$/d;/^#/d;s/\(.*\) ##\(.*\)#/\1/g;/\(.*\)localhost\(.*\)/d;s#127.0.0.1\ \(.*\)#\1#g;" "${filename}"
                ;;
            esac
        ;;
        "hosts-file.net")
            case "${ARG}" in
                "host"|"hosts"|"host0")
                    sed -i -e "/^$/d;/^#/d;/\(.*\)localhost\(.*\)/d;s/ \+//g" "${filename}"
                ;;
                *)
                    sed -i -e "/^$/d;/^#/d;/\(.*\)localhost\(.*\)/d;s#127.0.0.1\t\(.*\)#\1#g;s/ \+//g" "${filename}"
                ;;
            esac
        ;;
        "mirror1.malwaredomains.com")
            case "${file}" in
                "immortal_domains.zip")
                    sed -i -e "/^#/d;/^notice/d;s/ \+//g" "${filename}"
                ;;
                "malwaredomains.zones.zip")
                    #sed -i -e "/^\/\//d;s/ \+/ /g;s#zone \"\(.*\)\" {type master; file \"/etc/namedb/blockeddomain.hosts\";};#\1#g" "${filename}"
                    { rm "${filename}" && sed -e "/^\/\//d;s/ \+/ /g" | awk '{print $2}' | sed -e "s/\"//g" | sort -u -o "${filename}"; } < "${filename}"
                ;;
            esac
        ;;
        "someonewhocares.org")
            case "${ARG}" in
                "host"|"hosts"|"host0")
                    sed -i -e "/^#/d;/^\t\+#/d;s/^[ \t]*//g;/^ \+#/d;/\(.*\)local\(.*\)/d;/\(.*\)broadcast\(.*\)/d;s/\(.*\)#\(.*\)/\1/g;/^$/d" "${filename}"
                ;;
                *)
                    #sed -i -e "/^#/d;/^[[:space:]]*$/d;/\(.*\)local\(.*\)/d;/\(.*\)broadcast\(.*\)/d;s/\(.*\)#\(.*\)/\1/g;/^$/d;s#127.0.0.1 \(.*\)#\1#g" "${filename}"
                    { rm "${filename}" && sed -e "/^#/d;/^[[:space:]]*$/d;/\(.*\)local\(.*\)/d;/\(.*\)broadcast\(.*\)/d" | awk '{print $2}' | sort -u -o "${filename}"; } < "${filename}"
                ;;
            esac
        ;;
        "winhelp2002.mvps.org")
            case "${ARG}" in
                "host0")
                    sed -i -e "/^#/d;/^$/d;/^\r/d;/\(.*\)localhost\(.*\)/d;s/\(.*\)#\(.*\)/\1/g;s/ \+//g" "${filename}"
                ;;
                *)
                    sed -i -e "/^#/d;/^$/d;/^\r/d;/\(.*\)localhost\(.*\)/d;s#0.0.0.0 \(.*\)#\1#g;s/\(.*\)#\(.*\)/\1/g;s/ \+//g" "${filename}"
                ;;
            esac
        ;;
        "www.malwaredomainlist.com")
            case "${ARG}" in
                "host"|"hosts"|"host0")
                    #sed -i -e "/^$/d;/^\r/d;/^#/d;/^[[:space:]]*$/d;/\(.*\)localhost\(.*\)/d" "${filename}"
                    { rm "${filename}" && sed -e "/^#/d;/^[[:space:]]*$/d;/\(.*\)localhost\(.*\)/d" | awk '{print $2}' | sort -u -o "${filename}"; } < "${filename}"
                ;;
                *)
                    #sed -i -e "/^$/d;/^\r/d;/^#/d;/^[[:space:]]*$/d;/\(.*\)localhost\(.*\)/d;s#127.0.0.1  \(.*\)#\1#g" "${filename}" #;
                    { rm "${filename}" && sed -e "/^#/d;/^[[:space:]]*$/d;/\(.*\)localhost\(.*\)/d" | awk '{print $2}' | sort -u -o "${filename}"; } < "${filename}"
                ;;
            esac
        ;;
        *)  # delete empty lines, ^#, keep all others :p
            sed -i -e "/^$/d;/^#/d;s/ \+//g" "${filename}"
        ;;
    esac

    #cat "${filename}" | tr -d '[:space:]' > "${filename}"

}

transformer() {

    dialog --backtitle "${txt_project_name}" --title "${ttl_transformer}" --infobox "${txt_transform_file}'${DIR_SRC}/uniq_${list}'${txt_in_list}'${ARG}' \n\n ${txt_wait_minutes}" 10 100 
    
    case "${ARG}" in
        "bind"|"bind8"|"bind9")
            format="Bind Config"
            output="bind.zone"
        ;;
        "host"|"hosts"|"host0")
            format="hosts"
            output="hosts"
        ;;
        "unbound")
            format="Local zone"
            output="local-zone"
        ;;
        "pf")
			format="Packet-Filter"
			output="baddomains"
        ;;
    esac

    if [ -f "${DIR_SRC}/uniq_${list}" ];  then

        mssg="###########################
### ${ttl_project_name} ###
###########################
### ${txt_format}'${format}'
### ${txt_data}${now}
##
#"

        echo "${mssg}" > "${DIR_LISTS}/${output}"
        unset mssg
        
        case "${ARG}" in
			"host0")
                echo "0.0.0.0 localhost" >> "${DIR_LISTS}/${output}"
             ;;
             "host"|"hosts")
				if [ "${IPv4}" = 1 ]; then echo "127.0.0.1 localhost" >> "${DIR_LISTS}/${output}"; fi
				if [ "${IPv6}" = 1 ]; then echo "::1 localhost" >> "${DIR_LISTS}/${output}"; fi
             ;;
        esac

        i=0
        while read -r line; do
            line="$(echo "${line}" | tr -d '[:space:]')"    # replace '[:space:]' by '\040\011\012\015' for oldier version

            case "${ARG}" in
                "bind8")
                    echo "zone \"${line}\" { type master; notify no; file \"null.zone.file\"; };" >> "${DIR_LISTS}/${output}"
                ;;
                "bind"|"bind9")
                    echo "zone \"${line}\" { type master; notify no; file \"/etc/bind/nullzonefile.txt\"; };" >> "${DIR_LISTS}/${output}"
                ;;
                "host0")
                    echo "0.0.0.0 ${line}" >> "${DIR_LISTS}/${output}"
                ;;
                "host"|"hosts")
                    if [ "${IPv4}" = 1 ]; then echo "127.0.0.1 ${line}" >> "${DIR_LISTS}/${output}"; fi
                    if [ "${IPv6}" = 1 ]; then echo "::1 ${line}" >> "${DIR_LISTS}/${output}"; fi
                ;;
                "pf")
					echo "${line}"  >> "${DIR_LISTS}/${output}"
                ;;
                "unbound")
                    if [ "${IPv4}" = 1 ]; then 
						if [ "${USE_LZ_REDIRECT}" = 1 ]; then
							echo -e "local-zone: \"${line}\" redirect\nlocal-data: \"${line} A 127.0.0.1\"" >> "${DIR_LISTS}/${output}"; 
						else
							echo -e "local-data: \"${line} A 127.0.0.1\"" >> "${DIR_LISTS}/${output}";
						fi
					fi
                    if [ "${IPv6}" = 1 ]; then 
						if [ "${USE_LZ_REDIRECT}" = 1 ]; then
							echo -e "local-zone: \"${line}\" redirect\nlocal-data: \"${line} AAAA ::1\"" >> "${DIR_LISTS}/${output}"; 
						else
							echo -e "local-data: \"${line} AAAA ::1\"" >> "${DIR_LISTS}/${output}"; 
						fi
					fi
                ;;
            esac

            let i++
        done  < "${DIR_SRC}/uniq_${list}"
        unset i
        
        rm -f "${DIR_SRC}/uniq_${list}"

    else

        dialog --colors --title "${ttl_transformer}" --infobox "${txt_dlg_ko}${txt_error_no_file}'${DIR_SRC}/uniq_${list}'" 7 100
        sleep 1
        
        byebye

    fi

    unset arg

    if [ -f "${DIR_LISTS}/${output}" ]; then

        dialog --backtitle "${txt_project_name}" --colors --title "${ttl_transformer}" --infobox "${txt_dlg_ok}${txt_file}'${DIR_LISTS}/${output}'${txt_builded}" 7 100
		sleep 1

        build_sums

    else

		dialog --backtitle "${txt_project_name}" --colors --title "${ttl_transformer}" --infobox "${txt_dlg_ko}${txt_error_create_checksum_file}${DIR_LISTS}/${output}" 7 100
		sleep 1
		
        byebye

    fi

}
