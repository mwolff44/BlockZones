########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
# Date: 2017/07/16
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

list="bogons"

IPv6=0

FILES[0]="${DIR_LISTS}/fullbogons-ipv4.txt"
FILES[1]="${DIR_LISTS}/fullbogons-ipv6.txt"

########################################################################
###
##
#    Functions
##
###
########################################################################

. "${DIR_INC}/commons_functions.ksh"

# Create uniq list file by datas into array blocklist
mng_blocklists() {

    dialog --backtitle "${txt_project_name}" --title "${ttl_manager}" --infobox "${txt_read_array_bl}" 10 100

    count="${#blocklists[@]}"
    modulo=$(echo "100/$count"|bc)

    if [ "${count}" -gt 0 ]; then
    
		(

        for url in "${blocklists[@]}"; do

			file="$(echo "${url##*/}" | sed -e 's#\?#_#g;s#=#_#g;s#php#txt#g;s#\&#_#g')";
			filename="${DIR_DL}/${file}"
			
			let x+=$modulo
				
			cat <<EOF
XXX
$x
$txt_mng_list $list ($x%)
$txt_download $url
$txt_transform $file
XXX
EOF
			
            [ "${debug}" -eq 1 ] && printf "%s%s \n" "${txt_file}" "${file}"
            [ "${debug}" -eq 1 ] && printf "%s%s \n" "${txt_filename}" "${filename}"

            if [ -f "${filename}" ]; then

                # get file seconds stat
                file_seconds=$(stat -f "%m" -t "%s" "${filename}")

                # calcul diff time in seconds
                diff_sec=$(echo "${today} - ${file_seconds}" | bc)

                unset file_seconds

                if [ ${diff_sec} -gt ${seconds} ]; then

                    download

                fi

            else

                download

            fi
            
            if [ -f "${filename}" ]; then
            
				dialog --backtitle "${txt_project_name}" --colors --title "${ttl_manager}" --infobox "${txt_wait_download_file}${filename}" 10 100
				
				purge_files
				
				make_uniq_list
				
				if [ -f "${DIR_LISTS}/${file}" ]; then
				
					#[ "${verbose}" -eq 1 ] && display_mssg "OK" "The file '${DIR_LISTS}/${file}' seems to be build!"
					
					build_sums
				
				else
				
					dialog --backtitle "${txt_project_name}" --colors --title "${ttl_manager}" --infobox "${txt_dlg_ko}${txt_error}${txt_file}'${DIR_LISTS}/${file}'${txt_error_not_build}" 10 100
				
				fi
				
            fi 

            unset filename

        done
        
        ) | dialog --backtitle "${txt_project_name}" --title "${ttl_manager}" --gauge "${txt_wait}" 10 100 

    else

        dialog --backtitle "${txt_project_name}" --colors --title "${ttl_manager}" --infobox "${txt_dlg_ko}${txt_error_no_data}" 7 100
        sleep 1
        
        byebye

    fi

    unset count

}

purge_files() {

    #[ "${verbose}" -eq 1 ] && display_mssg "hb" "===> Attempt to transform downloaded file: ${filename}"

    # /^$/d     <= empty line
    # /^#/d     <= line starting with #
    # /^\s*$/d  <= remove spaces'n tabs; /^[[:space:]]*$/d
    # s/ \+//g  <= remove all spaces
    # s/ \+/ /g  <= replace more spaces by only one
    
    sed -i -e "/^$/d;/^#/d;/^[[:space:]]*$/d;" "${filename}"
    
    case "${file}" in
		"fullbogons-ipv4.txt")
			sed -i -e "s/192\.168\(.*\)/#192.168\1/" "${filename}"
		;;
		#"fullbogons-ipv6.txt")
			#sed -i -e "" "${filename}"
		#;;
    esac

}
