BlockZones
==========

Project to blacklist bad domains names, and bad adresses IP, *knew for bad activities*:
- ADS servers
- malwares, trackers, and others badlies
- bogons red

/!\ **July 2017 :: ATTENTION: SHA512 and sign files are created; see below howto using-it!** /!\

----------

Script 'bz_menu' 
----------------

Since July 2017: a new script named 'bz_menu' exists to execute all others with menu!

It exists only in pdksh version, for OpenBSD, and ksh93, for *BSD.

----------

Script 'badips'
--------------

=> Script 'badips' acts in several times:

- downloads wroten lists, into file 'src/badips', with a latence.
- threats them to create two single lists, one 'lists/badips\_ipv4 et one 'lists/badips\_ipv6, with sha512 checksums file

Copy both files where you want before manage them by pf!

=> IPv4 rules PF:

    table <t_badips> persist file "/dir/badips_ipv4"
    
    block drop in quick on egress from { <t_badips> } to any
    block drop out quick on egress from any to { <t_badips> }
    
=> IPV6 rules PF:
    
    table <t_badips6> persist file "/dir/badips_ipv6"
    
    block drop in quick on egress inet6 from { <t_badips6> } to any
    block drop out quick on egress inet6 from any to { <t_badips6> }

This script exists in pdksh, for OpenBSD, and ksh93, for *BSD.

/!\ Think to create a **daily**, weekly, or monthly task cron to update lists, and reload pf. /!\

**ATTENTION**: It seems necessary to grow the number entries tables...

----------

Script 'blacklist'
----------------

=> Script 'blacklist' acts in several times:

- downloads authorized lists, wroten into file 'src/domains', with a latence.
- creates one file:
  - 'lists/local-zone' to threat with unbound,
  - 'lists/bind.zone' to threat by bind (v8, v9),
  - 'lists/hosts' for local threat with '/etc/hosts'...
  - 'lists/baddomains' to threat with tables PF - only for *BSD 
     /!\ Be careful, PF needs more time, more power to convert domains in adresses IP... see this as Proof of Concept /!\
     /!\ If PF is not able to translate domain into adress IP, PF will not accept this list. /!\
- and create checksums files sha512!

Since January 2017: about 'unbound', use 'USE_LZ_REDIRECT' variable to manage 'local-zone "adr_ip" redirect' information... into 'inc/blacklists.ksh'

**ATTENTION**: If you active all wroten URL into 'src/domains' file, it possible the service that has to process the final single list can not do so because of a lack of memory resources.
Egual, this will grow threatment time to create single list by your computer. 

This script exists:

- bash version- *for Linux, in preference: Debian* -,  
- pdksh version- *for OpenBSD, preferably*, 
- ksh93 version- *for *BSD: FreeBSD, OpenBSD*

/!\ Think to create a weekly, **dialy** or monthly task cron to update informations; after, reload PF. /!\
     See a version file for OpenBSD, into 'cron/'.

=> The 'lists/personals' file exists to save yours personals choices to restrict some domains: one by line.

----------

Script 'bogons'
---------------

=> The 'bogons' script get both bogons (IPv4, IPv6) lists, availables by Team Cymry. It threats to manage them by PF- Packet Filter. 
After created them, see 'lists/':

- 'fullbogons-ipv4.txt' for ipv4
- 'fullbogons-ipv6.txt' for ipv6
- and checksums files

**pdksh (OpenBSD), ksh93 (*BSD) versions**
Copy the file where you want, before manage by PF.

**ATTENTION**: it seems necessary to grow the entries number of tables!

=> IPv4 rules PF:

    table <t_bogons> persist file "/dir/fullbogons-ipv4.txt"
    
    block drop in quick on egress from { <t_bogons> } to any
    block drop out quick on egress from any to { <t_bogons> }
    
=> IPv6 rules PF:
    
    table <t_bogons6> persist file "/dir/fullbogons-ipv6.txt"
    
    block drop in quick on egress inet6 from { <t_bogons6> } to any
    block drop out quick on egress inet6 from any to { <t_bogons6> }

**Bash (Linux) Version**

It's up to you... to manage with iptables!

*this is an example*:

    while read -r line; do
		/sbin/iptables -I INPUT -s "${line}" -j DROP
		/sbin/iptables -I OUTPUT -d "${line}" -j DROP
    done < /dir/BlockZones/lists/fullbogons-ipv4.txt
    
*Note: egual for bogons IPv6 list:*

    while read -r line; do
		/sbin/ip6tables -I INPUT -s "${line}" -j DROP
		/sbin/ip6tables -I OUTPUT -d "${line}" -j DROP
    done < /dir/BlockZones/lists/fullbogons-ipv6.txt

**Others informations:**

/!\ Think to create a monthly task cron to update informations, and after reload PF. /!\

----------

Config options
--------------

Somes options are setting into /dir/BlockZones/inc/vars.ksh file.

/!\ Set only them below; ATTENTION: others risk to create troubles. /!\

Since July 2017, it is possible with - *0 to disable; 1 to enable* -:

- to use Dialog interface 	- set variable 'dialog'; *default: 0*
- to use signify tool 		- set variable 'use_sign'; *default: 1*
- to create only one sign file, and checksums sha512 file - set variable 'one_checksum_file'; *default: 1*
- to use verbose mode 		- set variable 'verbose'; *default: 0*; **do not enable with dialog interface.**

Create lists
------------

./blacklist [option]: to create a list...

Options are:

- 'unbound',
- 'bind8' or 'bind9' 
- 'hosts' for the '/etc/hosts' file
- 'pf' for tables PF.

By default, the config of the 'src/domains' file is enough to manage correctly service as 'unbound'. 
It is possible you have lack of memories ressource, if you set all urls. If that is, reduce number urls.
<br/>
This "problem" not exists with 'hosts' file.

The default config manage quasi 65000 bad urls. The fully: ~ 500K!

**Thoses lists are updated all days, with checksums sha512 files, and sign files, at:**<br/>
[https://stephane-huc.net/share/BlockZones/lists/][1]

Check sign
----------

Since July 2017: Two checksums sha512 and sign files are created, about project code.

- 'BlockZones.pub': public key sign file, related to "BlockZones" Project,
- 'BlockZones.sha512': checksums [sha512][3] file, to verify all project files, 
- 'BlockZones.sha512.sign': sign file. 

To check good sign, use [signify(1)][2], at root project:

    $ signify -Cp BlockZones.pub -x BlockZones.sha512.sig

Of course, the 'signify' tool is available, by default, only on OpenBSD. 
Under Linux, scripts create only checksums sha512 files!

Managed lists
-------------

- 'immortals domains', 'malwaredomains' lists - Initiative DNS-BH Malwaredomains.com
- 'HpHosts' lists - hosts-file.net: **be carefull: automatic usage is forbiden!**
- 'Abuse' lists - abuse.ch
- 'malwaredomainlist' list - malwaredomainlist.com
- 'winhelp2002 MVPS' list  - winhelp2002.mvps.org
- 'pgl yoyo' list - pgl.yoyo.org
- 'adaway' list - adaway.org
- 'Dan Pollock' list - someonewhocares.org

----------

PF Notes
--------

Some notes, about Packet-Filter!

/!\ Think to create a regular task cron to flush tables PF. /!\

    # pfctl -t table_name -T expire nb_seconds

=> to reload one table, reload PF :

    # pfctl -f /etc/pf.conf

=> to grow entries number table, by edit '/etc/pf.conf' - just an example -:

    set limit table-entries 300000

=> If you use bogons, and badips lists, think to optimize yours rules PF - e.g:

    block drop in quick on egress from { <t_badips>, <t_bogons> } to any
    block drop out quick on egress from any to { <t_badips>, <t_bogons> }

And, same works for IPv6 rules!

----------

[1]: https://stephane-huc.net/share/BlockZones/lists/
[2]: http://man.openbsd.org/signify
[3]: http://man.openbsd.org/sha512
