#!/bin/ksh
#set -x
clear
########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
# Date: 2017/07/22
#
##
###
########################################################################
###
##
#   For OpenBSD: ksh - public domain Korn Shell
##
###
########################################################################

ROOT="$(dirname "$(readlink -f -- "$0")")"

. "${ROOT}/inc/vars.ksh"
[ $dialog = 1 ] && . "${DIR_INC}/blacklists_dialog.ksh" || . "${DIR_INC}/blacklists.ksh"

########################################################################
###
##
#    EXECUTION: DO NOT TOUCH!
##
###
########################################################################

. "${DIR_INC}/blacklists_execution.ksh"
